# Para levantar el servicio de Docker Registry
  
$ docker-compose up -d

# Subir tu propia images al registry local
Ejemplo
 
$ docker tag nginx-node:v0.1.0 localhost:5000/nginx-node:v0.1.0

$ docker push localhost:5000/nginx-node:v0.1.0




# EN CASO TENGO UN REGISTRY EN GCP
Ejemplo

$ docker tag [IMG_ID] gcr.io/[PROJECT_ID]/TAG_NAME

$ docker push gcr.io/[PROJECT_ID]/TAG_NAME
