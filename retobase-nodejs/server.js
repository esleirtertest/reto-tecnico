const _express = require('express');
const _server = _express();

const _port = 4000;

const _env = require('dotenv');
_env.config();

const _knex = require('knex')({
  client: 'mysql',
  connection: {
    host: 'mysql_db',
    user: 'root',
    password: 'sqStx32D2Kf6SShK',
    database: 'retobase',
    port: '3306'
  }
});

_server.get('/retoibm/sumar/:sumando01/:sumando02', async function(request, response) {
  try{
    var _sumando01 = new Number(request.params.sumando01);
    var _sumando02 = new Number(request.params.sumando02);
    var _resultado = _sumando01 + _sumando02;
    
    if (typeof _resultado !== "undefined" && _resultado!==null && !isNaN(_resultado)){  
      let result = await _knex.raw(`Insert Into sumas (sum1, sum2, result) values (${_sumando01}, ${_sumando02}, ${_resultado})`);
      console.log(result);
      if (result[0].affectedRows == 1) {
        return response.status(200).json({resultado : _resultado});
      } else {
        return response.status(500).json({resultado : "Server Error"});  
      }
    }else{
      return response.status(400).json({resultado : "Bad Request"});
    }
  }
  catch(e){
    return response.status(500).json({resultado : e});
  }
});


_server.listen(_port, () => {
   console.log(`Server listening at ${_port}`);
});

